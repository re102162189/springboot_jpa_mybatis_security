package com.demo.jdbc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.jdbc.core.RowMapper;

import com.demo.jpa.entity.Employee;
import com.demo.jpa.entity.Skill;

public class SkillRowMapper implements RowMapper<Optional<Skill>> {
	
	@Override
	public Optional<Skill> mapRow(ResultSet rs, int rowNum) throws SQLException {

		Set<Employee> employees = new HashSet<>();
		Skill skill = new Skill();
		skill.setEmployees(employees);
		
		do {
			if(skill.getId() == 0) {				
				skill.setId(rs.getLong("id"));
				skill.setName(rs.getString("name"));
			}
			
			Employee employee = new Employee();
			employee.setId(rs.getLong("e_id"));
			employee.setName(rs.getString("e_name"));
			employees.add(employee);
		} while (rs.next());
		
		return Optional.ofNullable(skill);
	}
}
