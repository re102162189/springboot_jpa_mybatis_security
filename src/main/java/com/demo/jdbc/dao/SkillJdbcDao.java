package com.demo.jdbc.dao;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.demo.jdbc.mapper.SkillRowMapper;
import com.demo.jpa.entity.Skill;

@Repository
public class SkillJdbcDao {

	private static final String SELECT = //
			"SELECT s.id AS id, s.name AS name," + //
					" e.id AS e_id, e.name AS e_name" + //
					" FROM skill s" + //
					" INNER JOIN" + //
					"		employee_skill es ON s.id=es.skill_id" + //
					" INNER JOIN" + //
					"		employee e ON e.id=es.employee_id" + //
					" WHERE s.id=:id";
	
	private static final String UPDATE = //
			"UPDATE skill SET name=:name WHERE id=:id";

	private static final String INSERT = //
			"INSERT INTO skill (name) VALUES (:name)";

	private static final String DELETE = //
			"DELETE FROM skill WHERE id=:id";

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Optional<Skill> findById(long id) {
		return namedParameterJdbcTemplate.queryForObject(SELECT, //
				new MapSqlParameterSource("id", id), new SkillRowMapper());
	}

	public int insert(Skill skill) {
		return namedParameterJdbcTemplate.update(INSERT, //
				new BeanPropertySqlParameterSource(skill));
	}

	public int update(Skill skill) {
		return namedParameterJdbcTemplate.update(UPDATE, //
				new BeanPropertySqlParameterSource(skill));
	}

	public int delete(long id) {
		return namedParameterJdbcTemplate.update(DELETE, //
				new MapSqlParameterSource("id", id));
	}
}
