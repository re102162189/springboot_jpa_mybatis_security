package com.demo.mybatis.mapper;

import java.util.Optional;

import com.demo.jpa.entity.Skill;

public interface SkillMapper {

	int insert(Skill skill);

	int update(Skill skill);

	int delete(long id);
	
	Optional<Skill> findById(long id);
}
