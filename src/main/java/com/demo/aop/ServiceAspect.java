package com.demo.aop;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Aspect
@Component
public class ServiceAspect {

	private static final Logger logger = LogManager.getLogger(ServiceAspect.class);

	@Autowired
	private ObjectMapper objectMapper;

	@Pointcut("execution(* com.demo.service.*.*(..))")
	public void serviceMethods() {
		// only use for AOP pointcut
	}

	@Before(value = "serviceMethods()")
	public void serviceBefore(JoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		String className = signature.getDeclaringType().getSimpleName();
		Method method = signature.getMethod();
		Parameter[] parameters = method.getParameters();
		Object[] args = joinPoint.getArgs();

		String paramsMapJsonString = getParamsMapJsonString(parameters, args);

		logger.info("@Before: {} params: {}", className, paramsMapJsonString);
	}

	@Around(value = "serviceMethods()")
	public 	Object serviceAround(ProceedingJoinPoint joinPoint) throws Throwable {
		
		logger.info("@Around: entering");
		
		String className = joinPoint.getSignature().getDeclaringType() + "." + joinPoint.getSignature().getName();

		long start = System.currentTimeMillis();
		Object output = joinPoint.proceed();
		long elapsedTime = System.currentTimeMillis() - start;

		logger.info("@Around: {}, execution time in {} milliseconds", className, elapsedTime);
		return output;
	}

	@After(value = "serviceMethods()")
	public void serviceAfter(JoinPoint joinPoint) {
		String className = joinPoint.getSignature().getDeclaringType() + "." + joinPoint.getSignature().getName();
		logger.info("@After: {}", className);
	}

	@AfterReturning(value = "serviceMethods()")
	public void serviceAfterReturning(JoinPoint joinPoint) {
		String className = joinPoint.getSignature().getDeclaringType() + "." + joinPoint.getSignature().getName();
		logger.info("@AfterReturning: {}", className);
	}

	@AfterThrowing(value = "serviceMethods()")
	public void serviceAfterThrowing(JoinPoint joinPoint) {
		String className = joinPoint.getSignature().getDeclaringType() + "." + joinPoint.getSignature().getName();
		logger.info("@AfterThrowing: {}", className);
	}

	private String getParamsMapJsonString(Parameter[] parameters, Object[] args) {
		Map<String, Object> params = new HashMap<>();
		for (int i = 0; i < parameters.length; i++) {
			params.put(parameters[i].getName(), args[i]);
		}
		try {
			return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(params);
		} catch (JsonProcessingException e) {
			logger.error("parse args to json error, args={}", args);
			return "";
		}
	}
}
