package com.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Realm {

	@Value("${security.write.username}")
	private String writeUsername;

	@Value("${security.write.password}")
	private String writePassword;

	@Value("${security.write.role}")
	private String writeRole;

	@Value("${security.read.username}")
	private String readUsername;

	@Value("${security.read.password}")
	private String readPassword;

	@Value("${security.read.role}")
	private String readRole;

	public String getWriteUsername() {
		return writeUsername;
	}

	public String getWritePassword() {
		return writePassword;
	}

	public String getWriteRole() {
		return writeRole;
	}

	public String getReadUsername() {
		return readUsername;
	}

	public String getReadPassword() {
		return readPassword;
	}

	public String getReadRole() {
		return readRole;
	}

}
