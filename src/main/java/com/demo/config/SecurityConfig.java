package com.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private Realm realm;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		String readUsername = realm.getReadUsername();
		String readPassword = encoder().encode(realm.getReadPassword());
		String readRole = realm.getReadRole();

		String writeUsername = realm.getWriteUsername();
		String writePassword = encoder().encode(realm.getWritePassword());
		String writeRole = realm.getWriteRole();

		UserDetails readUser = User.builder() //
				.username(readUsername) //
				.password(readPassword) //
				.roles(readRole) //
				.build();

		UserDetails writeUser = User.builder() //
				.username(writeUsername) //
				.password(writePassword) //
				.roles(writeRole, readRole) //
				.build();

		auth.inMemoryAuthentication()//
				.withUser(readUser)//
				.withUser(writeUser);//

//		auth.inMemoryAuthentication() //
//				.withUser(readUsername)//
//				.password(readPassword)//
//				.roles(readRole).and()//
//				.withUser(writeUsername)//
//				.password(writePassword)//
//				.roles(writeRole);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		String readRole = realm.getReadRole();
		String writeRole = realm.getWriteRole();

		http.httpBasic().and()//
				.authorizeRequests()//
				.antMatchers(HttpMethod.GET, "/rest/**").hasAnyRole(readRole, writeRole)//
				.antMatchers(HttpMethod.POST, "/rest/**").hasRole(writeRole)//
				.antMatchers(HttpMethod.PUT, "/rest/**").hasRole(writeRole)//
				.antMatchers(HttpMethod.DELETE, "/rest/**").hasRole(writeRole)//
				.and()//
				.sessionManagement()//
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)//
				.and()//
				.csrf().disable()//
				.cors().disable();
	}

	@Bean
	public PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}
}
