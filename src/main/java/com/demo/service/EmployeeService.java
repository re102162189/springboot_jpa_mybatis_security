package com.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.demo.jpa.entity.Employee;
import com.demo.jpa.repository.EmployeeRepository;

@Service
@Qualifier("employeeService")
public class EmployeeService implements IEmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Override
	public Optional<Employee> findById(long id) {
		return employeeRepository.findById(id);
	}

	@Override
	public Employee insert(Employee employee) {
		return employeeRepository.save(employee);
	}

	@Override
	public Employee update(Employee employee) {

		if (employeeRepository.existsById(employee.getId())) {
			return employeeRepository.save(employee);
		}

		return null;
	}

	@Override
	public boolean deleteById(long id) {
		if (employeeRepository.existsById(id)) {
			employeeRepository.deleteById(id);
		}
		
		return !employeeRepository.existsById(id);
	}
}
