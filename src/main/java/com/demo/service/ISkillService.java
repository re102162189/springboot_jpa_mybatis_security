package com.demo.service;

import java.util.Optional;

import com.demo.jpa.entity.Skill;

public interface ISkillService {
	Optional<Skill> findByIdJpa(long id);
	Optional<Skill> findByIdJpaQuery(long id);
	Optional<Skill> findByIdMapper(long id);
	Optional<Skill> findByIdJdbc(long id);
}