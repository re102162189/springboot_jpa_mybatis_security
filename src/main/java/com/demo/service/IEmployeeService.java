package com.demo.service;

import java.util.Optional;

import com.demo.jpa.entity.Employee;

public interface IEmployeeService {

	Optional<Employee> findById(long id);

	Employee insert(Employee employee);

	Employee update(Employee employee);

	boolean deleteById(long id);
}
