package com.demo.service;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.demo.jpa.entity.Employee;

@Service
@Qualifier("mockEmployeeService")
public class MockEmployeeService implements IEmployeeService {

	private static final Logger logger = LogManager.getLogger(MockEmployeeService.class);

	@Override
	public Optional<Employee> findById(long id) {
		Employee employee = new Employee();
		employee.setId(5);
		employee.setName("mock");
		return Optional.ofNullable(employee);
	}

	@Override
	public Employee insert(Employee employee) {
		employee.setId(0);
		employee.setName("mock");
		return employee;
	}

	@Override
	public Employee update(Employee employee) {
		employee.setId(1);
		employee.setName("mock");
		return employee;
	}

	@Override
	public boolean deleteById(long id) {
		logger.info("mock up id: {}", id);
		return id == 1;
	}
}
