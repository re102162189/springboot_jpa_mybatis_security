package com.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.jdbc.dao.SkillJdbcDao;
import com.demo.jpa.entity.Skill;
import com.demo.jpa.repository.SkillRepository;
import com.demo.mybatis.mapper.SkillMapper;

@Service
public class SkillService implements ISkillService {

	@Autowired
	private SkillRepository skillRepository;
	
	@Autowired
	private SkillMapper skillMapper;
	
	@Autowired
	private SkillJdbcDao skillJdbcDao;
	
	@Override
	public Optional<Skill> findByIdJpa(long id) {
		return skillRepository.findById(id);
	}
	
	@Override
	public Optional<Skill> findByIdJpaQuery(long id) {
		return skillRepository.findByIdUsingQuery(id);
	}
	
	@Override
	public Optional<Skill> findByIdMapper(long id) {
		return skillMapper.findById(id);
	}
	
	@Override
	public Optional<Skill> findByIdJdbc(long id) {
		return skillJdbcDao.findById(id);
	}
}
