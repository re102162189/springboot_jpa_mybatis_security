package com.demo.jpa.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.demo.jpa.entity.Skill;

public interface SkillRepository extends JpaRepository<Skill, Long> {
	@Query("SELECT s FROM Skill s JOIN FETCH s.employees e WHERE s.id = :id")
	Optional<Skill> findByIdUsingQuery(@Param("id") long id);
}
