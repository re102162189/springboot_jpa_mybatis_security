package com.demo.filter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Order(2)
@WebFilter(urlPatterns = "/rest/*")
@Component
public class AttributeFilter extends OncePerRequestFilter {

	private static final Logger logger = LogManager.getLogger(AttributeFilter.class);

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		Enumeration<String> attrNames = request.getAttributeNames();
		Map<String, Object> attributes = new HashMap<>();
		while(attrNames.hasMoreElements()) {
			String atrribute = attrNames.nextElement();
			attributes.put(atrribute, request.getAttribute(atrribute));
			
		}
		logger.info("attributes {}", attributes);
		
		filterChain.doFilter(request, response);
	}
}