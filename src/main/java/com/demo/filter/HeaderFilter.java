package com.demo.filter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Order(1)
@WebFilter(urlPatterns = "/rest/*")
@Component
public class HeaderFilter extends OncePerRequestFilter {

	private static final Logger logger = LogManager.getLogger(HeaderFilter.class);

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		Enumeration<String> headerNames = request.getHeaderNames();
		Map<String, String> headers = new HashMap<>();
		while(headerNames.hasMoreElements()) {
			String header = headerNames.nextElement();
			headers.put(header, request.getHeader(header));
			
		}
		logger.info("headers {}", headers);
		
		filterChain.doFilter(request, response);
	}
}