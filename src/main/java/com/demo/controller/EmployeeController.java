package com.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.jpa.entity.Employee;
import com.demo.service.IEmployeeService;

@RestController
@RequestMapping(value = "/rest/employee")
public class EmployeeController {

	@Autowired
	@Qualifier("employeeService")
	private IEmployeeService employeeService;

	@Autowired
	@Qualifier("mockEmployeeService")
	private IEmployeeService mockEmployeeService;

	@GetMapping(value = "/{id}")
	public ResponseEntity<Optional<Employee>> findById(@PathVariable("id") long id) {
		Optional<Employee> employee = employeeService.findById(id);
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Employee> insert(@RequestBody Employee employee) {
		Employee savedEmployee = employeeService.insert(employee);
		return new ResponseEntity<>(savedEmployee, HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<Employee> update(@RequestBody Employee employee) {
		Employee updatedEmployee = employeeService.update(employee);
		return new ResponseEntity<>(updatedEmployee, HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Boolean> deleteById(@PathVariable("id") long id) {
		boolean result = employeeService.deleteById(id);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	// ==================================mock=======================================

	@GetMapping(value = "/mock/{id}")
	public ResponseEntity<Optional<Employee>> findByIdMock(@PathVariable("id") long id) {
		Optional<Employee> employee = mockEmployeeService.findById(id);
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@PostMapping(value = "/mock")
	public ResponseEntity<Employee> insertMock(@RequestBody Employee employee) {
		Employee savedEmployee = mockEmployeeService.insert(employee);
		return new ResponseEntity<>(savedEmployee, HttpStatus.CREATED);
	}

	@PutMapping(value = "/mock")
	public ResponseEntity<Employee> updateMock(@RequestBody Employee employee) {
		Employee updatedEmployee = mockEmployeeService.update(employee);
		return new ResponseEntity<>(updatedEmployee, HttpStatus.OK);
	}

	@DeleteMapping(value = "/mock/{id}")
	public ResponseEntity<Boolean> deleteByIdMock(@PathVariable("id") long id) {
		boolean result = mockEmployeeService.deleteById(id);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
