package com.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.jpa.entity.Skill;
import com.demo.service.ISkillService;

@RestController
@RequestMapping(value = "/rest/skill")
public class SkillController {

	@Autowired
	private ISkillService skillService;

	@GetMapping(value = "/jpa/{id}")
	public ResponseEntity<Optional<Skill>> findByIdJpa(@PathVariable("id") long id) {
		Optional<Skill> skill = skillService.findByIdJpa(id);
		return new ResponseEntity<>(skill, HttpStatus.OK);
	}
	
	@GetMapping(value = "/jpa/query/{id}")
	public ResponseEntity<Optional<Skill>> findByIdJpaQuery(@PathVariable("id") long id) {
		Optional<Skill> skill = skillService.findByIdJpaQuery(id);
		return new ResponseEntity<>(skill, HttpStatus.OK);
	}
	
	@GetMapping(value = "/mybatis/{id}")
	public ResponseEntity<Optional<Skill>> findByIdMapper(@PathVariable("id") long id) {
		Optional<Skill> skill = skillService.findByIdMapper(id);
		return new ResponseEntity<>(skill, HttpStatus.OK);
	}
	
	@GetMapping(value = "/jdbc/{id}")
	public ResponseEntity<Optional<Skill>> findByIdJdbc(@PathVariable("id") long id) {
		Optional<Skill> skill = skillService.findByIdJdbc(id);
		return new ResponseEntity<>(skill, HttpStatus.OK);
	}
}
