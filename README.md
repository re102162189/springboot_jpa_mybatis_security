### Spring Boot
* [Spring Initializr](https://start.spring.io/)
* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.6.6/maven-plugin/reference/html/)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.6.6/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Security](https://docs.spring.io/spring-boot/docs/2.6.6/reference/htmlsingle/#boot-features-security)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.6.6/reference/htmlsingle/#boot-features-jpa-and-spring-data)

### Tomcat
* [Tomcat](https://tomcat.apache.org/download-80.cgi)

### JPA / Hibernate
* [OSIV](https://medium.com/@rafaelralf90/open-session-in-view-is-evil-fd9a21645f8e)
* [CascadeType/FetchType](https://openhome.cc/Gossip/EJB3Gossip/CascadeTypeFetchType.html)
* [lazyinitializationexception](https://matthung0807.blogspot.com/2020/11/spring-data-jpa-one-to-many-lazyinitializationexception.html)
* [JPA Concepts](https://tomee.apache.org/jpa-concepts.html)
* [N+1](https://www.javacodemonk.com/n-1-problem-in-hibernate-spring-data-jpa-894097b9)
* [find query executed](https://dataedo.com/kb/query/mysql/find-last-query-executed-by-session)

### mybatis
* [mybatis-spring-boot-autoconfigure](https://mybatis.org/spring-boot-starter/mybatis-spring-boot-autoconfigure/)

### DB/Table
```
docker run -d --name mysql \
	-e MYSQL_ROOT_PASSWORD=root \
	-e MYSQL_USER=admin \
	-e MYSQL_PASSWORD=admin \
	-p 3306:3306 mysql:5.7	
```

### Reference Documentation
* [jasypt](https://www.devglan.com/online-tools/jasypt-online-encryption-decryption)
```
-Djasypt.encryptor.password=1qaz2wsx
```